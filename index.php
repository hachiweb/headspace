<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Headspace</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="assets/js/costum.js"></script>
</head>

<body>
    <div class="d-flex justify-content-between bg-white d-block">
        <nav class="navbar navbar-expand-sm bg-white navbar-light">
            <!-- Brand/logo -->
            <a class="navbar-brand" href="">
                <img src="assets/images/logo.svg" alt="" class="w-50"></a>
            <!-- <a href="" class="navbar-brand"><img src="images/stwlogo.PNG" alt="" ></a> -->
        </nav>
        <nav class="navbar navbar-expand-sm bg-white navbar-light">
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <!-- Links -->
                <ul class="form-inline menu-txt">
                    <li class="">
                        <a class="" href="index-sample.php">THE SCIENCE</a>
                    </li>
                    <li class="">
                        <a class="" href="">BLOG</a>
                    </li>
                    <li class="">
                        <a class="" href="">FOR WORK</a>
                    </li>
                    <li class="">
                        <a class="" href="">HOW TO MEDITATE</a>
                    </li>
                    <li class="">
                        <a class="" href="">HELP <strong class=""> | </strong></a>
                    </li>
                    <li class="">
                        <a class="" href=""><strong>LOG IN</strong></a>
                    </li>
                    <li class="">
                        <button class="btn txt-btn">
                            <a class=" text-white text-decoration-none" href="">SIGN UP FOR FREE</a>
                        </button>
                    </li>
                </ul>
            </div>

        </nav>
    </div>
    <section style="background-image:url('assets/images/background.svg');background-repeat: no-repeat;background-position: center;">
        <div class="container-fluid">
            <div class="row" style="padding-top:100px;">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <img src="assets/images/boombox.svg" alt="">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 text-center">
                    <h2>Your guide to health and happiness</h2>
                    <p>Live a healthier, happier, more well-rested life with Headspace.</p>
                    <div class="text-center">
                        <button class="btn txt-btn-1 w-50"><a href="#" class="txt-1 txt text-decoration-none">SIGN UP
                                FOR
                                FREE</a></button>
                        <button class="btn txt-btn-2 w-50"><a href="#" class="txt-1 txt text-decoration-none">SUBSCRIBE
                                NOW</a></button>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <img src="assets/images/swings.svg" alt="" style="width:50%;">
                </div>
            </div>
        </div>
 
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <img src="assets/images/bikers.svg" alt="" style="width:70%;">
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 text-center">
                    <img src="assets/images/orange-breathing-character.svg" alt="" style="width:30%;">
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12 text-center">
                    <img src="assets/images/treeguy.svg" alt="" style="width:70%;">
                </div>
            </div>
        </div>
        
            <img src="assets/images/header-hills-fg.svg" alt="" class="w-100" style="margin-bottom:-2px !important">
         
        </section>
        <div class="" style="background-color:rgb(129,141,171);">
            <div class="container-fluid">
                <div class="row personal-txt-1">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 px-5">
                        <h3 class="txt1">Learn to meditate and live mindfully</h3>
                        <ul class="txt-ul">
                            <li>Hundreds of themed sessions on everything from
                                <a href="" class="txtcent-2 text-decoration-none">stress</a> and
                                <a href="" class="txtcent-2 text-decoration-none">sleep</a> to
                                <a href="" class="txtcent-2 text-decoration-none">focus</a> and
                                <a href="" class="txtcent-2 text-decoration-none">anxiety</a></li>
                            <li>Bite-sized <a href="" class="txtcent-2 text-decoration-none"> guided meditations</a> for
                                busy schedules</li>
                            <li>SOS exercises in case of sudden meltdowns</li>
                        </ul>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12  text-center">
                        <img src="assets/images/train-cat.svg" alt="" class="w-50">
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row personal-txt">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12  text-center">
                        <img src="assets/images/train-cat.svg" alt="" class="w-50">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12" >
                        <h2 class="txt1">A personal meditation guide, right in your pocket</h2>
                        <!-- <div style="background-image:url('assets/images/how-it-works.webp');width:50%;">
                    <h1>shauken</h1>
                    </div> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="" style="background-color:rgb(123,205,200);">
            <div class=""
                style="background-image:url('assets/images/zany-bg-tranparent.svg');background-repeat: no-repeat;background-size:cover;background-position: center bottom;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 txtcent-meditation">
                            <h2>Stressed out?</h2>
                            <h6><a href="">Meditation</a> has been shown to reduce daily<br> stress and perceived <a
                                    href="">stress</a>.1</h6>
                            <button class="btn txt-btn-1"><a href="#" class="txt-1 txt text-decoration-none">SIGN
                                    UP FOR
                                    FREE</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="" style="background-color:rgb(85,154,209);">
            <div class=""
                style="background-image:url('assets/images/zany-bg-tranparent.svg');background-repeat: no-repeat;background-size:cover;background-position: center bottom;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 txtcent txtcent-meditation">
                            <h2>Stay focused</h2>
                            <h6><a href="" class="txtcent-1 text-decoration-none">Mindfulness</a> meditation has been
                                shown to promote subtle improvements in<br><a href=""
                                    class="txtcent-1 text-decoration-none">focus</a>, attention and the ability to
                                ignore distractions.2</h6>
                            <button class="btn txt-btn-3"><a href="#" class="txt-1 txt2 text-decoration-none">SUBSCRIBE
                                    NOW</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="" style="background-color:rgb(255,143,152);">
            <div class=""
                style="background-image:url('assets/images/zany-bg-tranparent.svg');background-repeat: no-repeat;background-size:cover;background-position: center bottom;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 txtcent-meditation">
                            <h2>Boost compassion</h2>
                            <h6>Meditating with Headspace for three weeks <a href="">may enhance</a><br> <a
                                    href="">compassionate behavior</a> toward others.3</h6>
                            <button class="btn txt-btn-1"><a href="#" class="txt-1 txt text-decoration-none">SIGN
                                    UP FOR
                                    FREE</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="" style="background-color:rgb(239,236,231);">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="back-img">
                            <img class="set-image" src="assets/images/dotcom-home-screen.svg" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 txtcent-meditation-1 mt-5">
                        <h1>Try Headspace® for free</h1>
                        <h6>Download the Headspace app or sign up online to start meditating today.</h6>
                        <div class="text-center">
                            <button class="btn txt-btn-1 w-25"><a href="#" class="txt-1 txt text-decoration-none">GET
                                    STARTED</a></button><br>
                            <a href=""><img class="w-25 m-3" src="assets/images/app-store.png" alt=""></a>
                            <a href=""><img class="w-25" src="assets/images/google-play.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
    <section class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 references">
                <h4>References</h4>
                <p><span class="font">1</span>. Jayewardene, W., Lohrmann, D., Erbe, R., & Torabi, M. (2017). Effects of
                    preventative online mindfulness interventions on stress and mindfulness: A meta-analysis of
                    randomized controlled trials.</p>
                <p class="mt-3"><span class="font">3</span>. Lim, D., Condon, P., & DeSteno, D. (2015). Mindfulness and
                    compassion: an examination of mechanism and scalability.</p>
                <p class="mt-3">PloS one, 10(2), e0118221.</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 references mt-3">
                <p class="mt-3"><span class="font">2</span>. Sedlmeier, P., et al. The Psychological Effects of
                    Meditation: A Meta-Analysis.</p>
                <p class="mt-3">Psychological Bulletin, 2012, Vol. 138, No. 6, 1139 –1171</p>
            </div>
        </div>
    </section>
    <section class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 references-1 ">
                <span class="text-dark">GET SOME HEADSPACE</span>
                <ul class="">
                    <li><a class="text-decoration-none" href="" data-testid="">SUBSCRIBE</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">REDEEM A CODE</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">SEND A GIFT</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">GUIDED MEDITATION</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">MEDITATION FOR WORK</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">MEDITATION FOR KIDS</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">MEDITATION ON SLEEP</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">MEDITATION ON FOCUS</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">MEDITATION ON STRESS</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">MEDITATION ON ANXIETY</a></li>
                </ul>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12  references-1">
                <span class="text-dark">OUR COMMUNITY</span>
                <ul>
                    <li><a class="text-decoration-none" href="" data-testid="">BLOG</a></li>
                </ul>
                <span class="text-dark">ABOUT US</span>
                <ul>
                    <li><a class="text-decoration-none" href="" data-testid="">ABOUT HEADSPACE</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">ABOUT ANDY PUDDICOMBE</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">ANDY'S BOOKS</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">PRESS &amp; MEDIA</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">CAREERS</a></li>
                </ul>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 references-1">
                <span class="text-dark">SUPPORT</span>
                <ul class="pt-0">
                    <li><a class="text-decoration-none" href="" data-testid="">HELP CENTER</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">CONTACT US</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">ALEXA</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">GOOGLE ASSISTANT</a></li>

                </ul>
                <span class="text-dark">PARTNERSHIPS</span>
                <ul>
                    <li><a class="text-decoration-none" href="" data-testid="">RESEARCH PARTNERS</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">BRAND PARTNERS</a></li>
                    <li><a class="text-decoration-none" href="" data-testid="">PHILANTHROPY</a></li>
                </ul>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 referencess">
                <span class="text-dark d-block">GET THE APP</span>
                <a href=""><img src="assets/images/app-store.png" alt=""></a>
                <a href=""><img src="assets/images/google-play.png" alt=""></a>

            </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-sm-12 references-2 text-center">
            <ul class="form-inline">
                    <li><a class="text-decoration-none" href="" ><i class="fab fa-instagram fa-2x"></i></a></li>
                    <li><a class="text-decoration-none" href="" ><i class="fab fa-twitter fa-2x"></i></a></li>
                    <li><a class="text-decoration-none" href="" ><i class="fab fa-facebook-f fa-2x"></i></a></li>
                    <li><a class="text-decoration-none" href="" ><i class="fab fa-youtube fa-2x"></i></a></li>
                    <li><a class="text-decoration-none" href="" ><i class="fab fa-linkedin fa-2x"></i></a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="container">
        <div class="row">
            <div class="col-sm-12 text-center footer-set">
                <a class="" >© 2019 HEADSPACE INC.</a>
                <a class="" href="" >TERMS &amp; CONDITIONS</a>
                <a class="" href="" >PRIVACY POLICY</a>
                <a class="" href="" >COOKIE POLICY</a>
                <a class="" href="" >SITE MAP</a>
                <a class="h6" >v1.19.36</a>
    
            </div>
        </div>
    </section>

</body>

</html>